package labClin.Entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Protocolo {
	private long numero;
	private Date data;
	List<Exame> exames = new ArrayList<Exame>();
	
	
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	public void addExame(Exame exame) {
		exames.add(exame);
	}
	
	public double total() {
		double soma = 0;
		
		for (Exame exame : exames) {
			soma += exame.getValor();
		}
		
		return soma;
	}
}
