package labClin.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	private String nome;
	private String CPF;
	
	List<Protocolo> protocolos = new ArrayList<Protocolo>();
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCPF() {
		return CPF;
	}
	public void setCPF(String cPF) {
		CPF = cPF;
	}
	
	public void addProtocolo(Protocolo protocolo) {
		protocolos.add(protocolo);
	}
	
	public Protocolo getProtocolo(long numProtocolo) {
		
		//TODO Essa pesquisa é tosca. Tem que melhorar
		for (Protocolo protocolo : protocolos) {
			if(protocolo.getNumero() == numProtocolo) {
				return protocolo;
			}
		}
		
		return null;
	}
}
