package labClin.Entidades.Test;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import labClin.Entidades.Cliente;
import labClin.Entidades.Exame;
import labClin.Entidades.Protocolo;

public class ProtocoloTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Cliente cliente;
		Protocolo protocolo;
		Exame exame;
		
		cliente = new Cliente();
		cliente.setNome("Menino Maluquinho");
		cliente.setCPF("111111111-11");
		
		protocolo = new Protocolo();
		protocolo.setNumero(1);
		protocolo.setData(Calendar.getInstance().getTime());
		cliente.addProtocolo(protocolo);
		
		exame = new Exame();
		exame.setDescricao("Hemograma");
		exame.setValor(12.5);
		protocolo.addExame(exame);
		
		exame = new Exame();
		exame.setDescricao("Gama GT");
		exame.setValor(5.75);
		protocolo.addExame(exame);

		assertEquals(18.25, cliente.getProtocolo(10).total(), 0.005);
	}

}















